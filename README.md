### Hi there 👋

 🙋 Developer and linux enthusiast.

<h3>⚙️ Stack</h3>

 💻 Javascript, C/C++, Rust, Ansible, Docker, Shell script
 
 💲 Arch Linux, Awesomewm, Tmux, Neovim
 <!-- [![Fabio Kleis Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=fabiokleis&layout=compact&theme=tokyonight)](https://github.com/fabiokleis) -->
 
<h3>🛠️ Projects</h3>

 🐧 Fabiokleis - [@fabiokleis](https://fabiokleis.herokuapp.com)
 
 🎮 c++ game - [@sfml_game](https://github.com/fabiokleis/sfml_game)
